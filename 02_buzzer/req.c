#include<stdio.h>
#define BV(n)  (1<<(n))
void check_bit(int num);
void set_bit(int num);
void clear_bit(int num);
void toggle_bit(int num);
void bits_num(int num);


int main(void)
{
	int num;
	printf("Enter a number:\n");
	scanf("%d",&num);
	check_bit(num);
	bits_num(num);
	set_bit(num);
	clear_bit(num);
	toggle_bit(num);
	return 0;
}


void check_bit(int num)
{
	int	val=3;
	if((num&BV(2)))
		printf("1\n");
	else 
		printf("0\n");

		
}

void set_bit(int num)
{

	num|=BV(3);
	bits_num(num);
}

void clear_bit(int num)
{
	
	num&=~BV(3);
	bits_num(num);
}
void toggle_bit(int num)
{
	num^=BV(3);
	bits_num(num);
	

}

void bits_num(int num)
{
	unsigned int mask=0x80000000;
	while(mask)
	{
		if((num&mask)==0)
			printf("0");
		else
			printf("1");
		mask=mask>>1;
	}
	printf("\n");
}

