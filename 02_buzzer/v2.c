#include"LPC17xx.h"

#define BUZZER   11

void buz_init(void);
void buz_on(void);
void buz_off(void);
void beep(uint32_t delay);

int main(void)
{
	int i;
	buz_init();
	for(i=1;i<3;i++)
	{
		beep(2000);
		delay_ms(2000);
	}
	return 0;

}
void buz_init(void)
{
	//P2.11	output
	LPC_GPIO2->FIODIR|=BV(BUZZER);
	//off the buzzer
	buz_off();
}
void buz_on(void)
{
	//P2.11 CLR bcoz pnp transistor
	LPC_GPIO2->FIOCLR |= BV(BUZZER);
}
void buz_off(void)
{
	//P2.11 SET=1 
	LPC_GPIO2->FIOSET |= BV(BUZZER);
}
void beep(uint32_t delay)
{

	buz_on();
	delay_ms(delay);
	buz_off();

}
