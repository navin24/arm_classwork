#include"LPC17xx.h"

#define LED   29

#define BUZZER   11

void buz_init(void);
void buz_on(void);
void buz_off(void);
void beep(uint32_t delay);
void led_init(void);
void led_on(void);
void led_off(void);

int main(void)
{
	int i;
	buz_init();
	led_init();
												
	for(i=1;i<3;i++)
	{
		led_on();
		beep(2000);
		delay_ms(2000);
		led_off();
		delay_ms(2000);
	}
	return 0;

}
void led_init(void)
{
	//P1.29	output
	LPC_GPIO1->FIODIR|=BV(LED);
	//off the led
	led_off();
}
void led_on(void)
{
	//P1.29 SET=1
	LPC_GPIO1->FIOSET|=BV(LED);
}
void led_off(void)
{
	//P1.29 CLR=1 
	LPC_GPIO1->FIOCLR|=BV(LED);

}

void buz_init(void)
{
			//P2.11	output
		LPC_GPIO2->FIODIR|=BV(BUZZER);
			//off the buzzer
		buz_off();
}
void buz_on(void)
{
		//P2.11 CLR bcoz pnp transistor
	LPC_GPIO2->FIOCLR |= BV(BUZZER);
}
void buz_off(void)
{
	//P2.11 SET=1 
	LPC_GPIO2->FIOSET |= BV(BUZZER);
}
void beep(uint32_t delay)
{
	buz_on();
	delay_ms(delay);
	buz_off();
}
