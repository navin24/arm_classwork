#include"LPC17xx.h"

#define LED   29

void led_init(void);
void led_on(void);
void led_off(void);

int main(void)
{
	int i;
	led_init();
	for(i=1;i<3;i++)
	{
		led_on();
		delay_ms(10);
		led_off();
		delay_ms(10);
	}
	return 0;

}
void led_init(void)
{
	//P1.29	output
	LPC_GPIO1->FIODIR|=BV(LED);
	//off the led
	led_off();
}
void led_on(void)
{
	//P1.29 SET=1
	LPC_GPIO1->FIOSET|=BV(LED);
}
void led_off(void)
{
	//P1.29 CLR=1 
	LPC_GPIO1->FIOCLR|=BV(LED);
}
