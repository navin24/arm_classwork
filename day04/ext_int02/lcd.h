#ifndef __LCD_H
#define __LCD_H

#include "LPC17xx.h"

#define LCD_DATA_PORT	LPC_GPIO2
#define LCD_CTRL_PORT	LPC_GPIO1

#define LCD_D4		4
#define LCD_D5		5
#define LCD_D6		6
#define LCD_D7		7

#define LCD_EN		22
#define LCD_RW		23
#define LCD_RS		24

#define LCD_CLEAR		0x01
#define LCD_ENTRY_MODE	0x06
#define LCD_DISPLAY_ON	0x0C
#define LCD_FUNC_SET	0x28
#define LCD_LEFT_SHIFT_DISPLAY	0x18

#define LCD_LINE1	0x80
#define LCD_LINE2	0xC0

void lcd_busy_wait(void);
void lcd_write_nibble(uint8_t val);
void lcd_write_data(uint8_t val);
void lcd_write_cmd(uint8_t val);
void lcd_puts(uint8_t line, char *str);
void lcd_init(void);

#endif

