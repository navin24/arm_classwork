#ifndef __EINT2_H
#define __EINT2_H

#include "LPC17xx.h"

#define EXT2 2

void eint2_init(void);
extern int eint2_cnt;
extern volatile int eint_flag;

#endif
