#include "lcd.h"

void lcd_busy_wait(void)
{
	// make D7 as input pin
	LCD_DATA_PORT->FIODIR &= ~BV(LCD_D7);
	// make RS=0
	LCD_CTRL_PORT->FIOCLR = BV(LCD_RS);
	// make RW=1, EN=1
	LCD_CTRL_PORT->FIOSET |= BV(LCD_RW) | BV(LCD_EN);
	// read D7, until it is 0
	while( (LCD_DATA_PORT->FIOPIN & BV(LCD_D7)) != 0 )
		;
	// make EN=0
	LCD_CTRL_PORT->FIOCLR = BV(LCD_EN);
	// make D7 as output pin
	LCD_DATA_PORT->FIODIR |= BV(LCD_D7);
}

void lcd_write_nibble(uint8_t val)
{
	// make RW=0
	LCD_CTRL_PORT->FIOCLR = BV(LCD_RW);
	// write data on d4-d7
	LCD_DATA_PORT->FIOCLR = BV(LCD_D4) | BV(LCD_D5) | BV(LCD_D6) | BV(LCD_D7);
	LCD_DATA_PORT->FIOSET |= ((uint32_t)val) << LCD_D4;
	// falling edge on en
	LCD_CTRL_PORT->FIOSET |= BV(LCD_EN);
	// delay_ms(1);
	__NOP();	//asm("NOP");
	LCD_CTRL_PORT->FIOCLR = BV(LCD_EN);
}

void lcd_write_data(uint8_t val)
{
	uint8_t high = val >> 4, low = val & 0x0F;
	// make RS=1
	LCD_CTRL_PORT->FIOSET |= BV(LCD_RS);
	// write upper nibble
	lcd_write_nibble(high);
	// write lower nibble
	lcd_write_nibble(low);
	// wait for busy flag
	lcd_busy_wait();
	delay_ms(3);
}

void lcd_write_cmd(uint8_t val)
{
	uint8_t high = val >> 4, low = val & 0x0F;
	// make RS=0
	LCD_CTRL_PORT->FIOCLR = BV(LCD_RS);
	// write upper nibble
	lcd_write_nibble(high);
	// write lower nibble
	lcd_write_nibble(low);
	// wait for busy flag
	lcd_busy_wait();
	delay_ms(3);
}

void lcd_puts(uint8_t line, char *str)
{
	int i;
	// set line address
	lcd_write_cmd(line);
	// write char data one by one
	for(i=0; str[i]!='\0'; i++)
		lcd_write_data(str[i]);
}

void lcd_init(void)
{
	// make all data pins as output
	LCD_DATA_PORT->FIODIR |= BV(LCD_D4) | BV(LCD_D5) | BV(LCD_D6) | BV(LCD_D7);
	// make all control pins as output
	LCD_CTRL_PORT->FIODIR |= BV(LCD_RS) | BV(LCD_RW) | BV(LCD_EN);
	
	lcd_busy_wait();
	//delay_ms(200);

	// set 4-bit mode
	lcd_write_cmd(LCD_FUNC_SET);
	// display on
	lcd_write_cmd(LCD_DISPLAY_ON);
	// clear display
	lcd_write_cmd(LCD_CLEAR);
	// set entry mode
	lcd_write_cmd(LCD_ENTRY_MODE);
}

