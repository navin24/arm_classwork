#include "LPC17xx.h"
#include "lcd.h"
#include "eint2.h"
#include <stdio.h>

int main(void)
{
	lcd_init();
	lcd_puts(LCD_LINE1,"'Interrupt Demo'");
	eint2_init();
	
	while(1)
	{
		while(eint_flag==0)
			;
		eint_flag = 0;

		char str[20];
		eint2_cnt++;
		sprintf(str,"Int Count:	%2d",eint2_cnt);
		lcd_puts(LCD_LINE2,str);
		
	}


	return 0;
}


