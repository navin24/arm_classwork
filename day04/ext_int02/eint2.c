#include "LPC17xx.h"
#include "eint2.h"
#include "lcd.h"

//int eint2_cnt = 0;
volatile int eint_flag =0;

void eint2_init(void)
{
	LPC_PINCON->PINSEL4 &= ~(BV(25) | BV(24));
	LPC_PINCON->PINSEL4 |= BV(24);

	LPC_SC->EXTMODE |= BV(EXT2);
	LPC_SC->EXTPOLAR |= BV(EXT2);
	LPC_SC->EXTINT |= BV(EXT2);

	NVIC_EnableIRQ(EINT2_IRQn);
}
void EINT2_IRQHandler(void)
{
	eint_flag =1;
	
	LPC_SC->EXTINT |= BV(EXT2);
}
