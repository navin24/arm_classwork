#include "uart.h"


void uart_init(uint32_t baud)
{
	uint16_t dl;
	LPC_PINCON->PINSEL0 &= ~(BV(3) | BV(2) | BV(1) | BV(0) );
	LPC_PINCON->PINSEL0 |= (BV(3) | BV(1));
	LPC_UART3->LCR =  BV(LCR_DLAB) | BV(LCR_DL1) | BV(LCR_DL0) ;
	LPC_UART3->FCR |= BV(FIFO_EN);
	dl = (PCLK/16) /baud;
	LPC_UART3->DLL=dl&0x00FF;
	LPC_UART3->DLM=dl>>8;
	LPC_UART3->LCR &= ~BV(LCR_DLAB);
}

void uart_putch(int ch)
{
	while((LPC_UART3->LSR & BV(LSR_THRE))==0)
		;
	LPC_UART3->THR = ch;
}
int uart_getch(void)
{
	while((LPC_UART3->LSR & BV(LSR_RDR) )==0)
		;
	return (int)LPC_UART3->RBR;

}
void uart_puts(char str[])
{
	int i;
	for(i=0;str[i]!='\0';i++)
	{
		uart_putch(str[i]);
	}
}
void uart_gets(char str[])
{
	int ch , i=0;
	do{
		ch = uart_getch();
		str[i++]=ch;
	}while(ch!='\r');
	str[i++]='\n';
	str[i]='\0';

}

