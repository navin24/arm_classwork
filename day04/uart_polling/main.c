#include "LPC17xx.h"
#include "uart.h"
#include <stdio.h>
#include <string.h>

int main(void)
{
	char str[32];
	uart_init(9600);
	uart_puts("UART DEMO\r\n");
	uart_puts("Enter a String:\r\n");
	while(1)
	{
		uart_gets(str);
		strupr(str);
		uart_puts(str);
	}	
	return 0;
}


