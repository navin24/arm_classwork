#include "timer.h"

volatile int timer0_mr0_intr = 0;

void timer0_init(uint32_t ms) {
	uint32_t cnt;
	// disable clock and reset timer registers (TCR)
	LPC_TIM0->TCR = BV(TCR_RST);
	// set mode as timer mode (CTCR)
	LPC_TIM0->CTCR = CTCR_TIMER_MODE;
	// set prescalar (PR)
	LPC_TIM0->PR = TIM0_PR_VAL-1;
	// calculate number of clock cycles for given delay and set in MR0
	cnt = (PCLK / 1000) * ms / 18;
	LPC_TIM0->MR0 = cnt-1;
	// enable interrupt on MR0 match and reset the timer -- peripheral
	LPC_TIM0->MCR |= BV(MCR_MR0_I) | BV(MCR_MR0_R);
	// enable interrupt in NVIC
	NVIC_EnableIRQ(TIMER0_IRQn);
	// enable timer clock
	LPC_TIM0->TCR = BV(TCR_CLKEN);
}

void TIMER0_IRQHandler(void) {
	// set timer interrupt flag
	timer0_mr0_intr = 1;
	// clear the interrupt (IR)
	LPC_TIM0->IR = BV(IR_MR0_I);
}

