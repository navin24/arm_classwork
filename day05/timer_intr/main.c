#include "LPC17xx.h"
#include "lcd.h"
#include "timer.h"
#include <stdio.h>

int main()
{
	char str[20];
	int count = 0;
	lcd_init();
	lcd_puts(LCD_LINE1, "SUNBEAM DESD LCD");
	lcd_puts(LCD_LINE2, "Count = 000");
	timer0_init(3000);
	while(1) {
		// wait for timer interrupt
		while(timer0_mr0_intr == 0)
			;
		timer0_mr0_intr = 0;
		// increment count and display on lcd
		count++;
		sprintf(str, "Count = %03d", count);
		lcd_puts(LCD_LINE2, str);
	}
	return 0;
}

