#ifndef __TIMER_H
#define __TIMER_H

#include "LPC17xx.h"

#define CTCR_TIMER_MODE		0x00

#define TCR_CLKEN		0
#define TCR_RST			1

#define MCR_MR0_I			0
#define MCR_MR0_R			1
#define MCR_MR0_S			2
#define MCR_MR1_I			3
#define MCR_MR1_R			4
#define MCR_MR1_S			5
#define MCR_MR2_I			6
#define MCR_MR2_R			7
#define MCR_MR2_S			8
#define MCR_MR3_I			9
#define MCR_MR3_R			10
#define MCR_MR3_S			11

#define IR_MR0_I			0
#define IR_MR1_I			1
#define IR_MR2_I			2
#define IR_MR3_I			3
#define IR_CAP0_I			4
#define IR_CAP1_I			5

#define TIM0_PR_VAL			18

extern volatile int timer0_mr0_intr;

void timer0_init(uint32_t ms);
void TIMER0_IRQHandler(void);

#endif

