#include "rtc.h"

void rtc_init(rtc_time_t *tm) {
	// enable rtc clock
	LPC_RTC->CCR = BV(CCR_CLKEN);
	// set rtc registers
	rtc_set(tm);
}

void rtc_set(rtc_time_t *tm) {
	// disable rtc clock & reset registers
	LPC_RTC->CCR = BV(CCR_CLKRST);
	// set rtc registers
	LPC_RTC->SEC = tm->sec;
	LPC_RTC->MIN = tm->min;
	LPC_RTC->HOUR = tm->hour;
	LPC_RTC->DOM = tm->day_of_month;
	LPC_RTC->MONTH = tm->month;
	LPC_RTC->YEAR = tm->year;
	LPC_RTC->DOY = tm->day_of_year;
	LPC_RTC->DOW = tm->day_of_week;
	// enable rtc clock
	LPC_RTC->CCR = BV(CCR_CLKEN);
}

rtc_time_t rtc_get(void) {
	// get rtc registers
	rtc_time_t tm;
	tm.sec = LPC_RTC->SEC;
	tm.min = LPC_RTC->MIN;
	tm.hour = LPC_RTC->HOUR;
	tm.day_of_month = LPC_RTC->DOM;
	tm.month = LPC_RTC->MONTH;
	tm.year = LPC_RTC->YEAR;
	tm.day_of_year = LPC_RTC->DOY;
	tm.day_of_week = LPC_RTC->DOW;

	return tm;
}
