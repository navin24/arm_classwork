#include "LPC17xx.h"
#include "lcd.h"
#include "rtc.h"
#include <stdio.h>

int main()
{
	char str[20];
	rtc_time_t tm = { .year=2020, .month=12, .day_of_month=13, .hour=11, .min=20, .sec=0, .day_of_week=0, .day_of_year=348 };
	lcd_init();
	rtc_init(&tm);
	while(1) {
		tm = rtc_get();
		sprintf(str, "RTC: %02lu-%02lu-%04lu", tm.day_of_month, tm.month, tm.year);
		lcd_puts(LCD_LINE1, str);
		sprintf(str, "%02lu:%02lu:%02lu WKDY:%lu", tm.hour, tm.min, tm.sec, tm.day_of_week);
		lcd_puts(LCD_LINE2, str);
		delay_ms(1000);
	}
	return 0;
}

