#include "uart.h"
#include<stdio.h>

static char *tx_str = NULL;
static volatile int tx_completed = 1;
static int tx_index = 0;

void uart_init(uint32_t baud)
{
	uint16_t dl;
	LPC_PINCON->PINSEL0 &= ~(BV(3) | BV(2) | BV(1) | BV(0) );
	LPC_PINCON->PINSEL0 |= (BV(3) | BV(1));
	LPC_UART3->LCR =  BV(LCR_DLAB) | BV(LCR_DL1) | BV(LCR_DL0) ;
	LPC_UART3->FCR |= BV(FIFO_EN);
	dl = (PCLK/16) /baud;
	LPC_UART3->DLL = dl & 0x00FF;
	LPC_UART3->DLM = dl >> 8;
	LPC_UART3->LCR &= ~BV(LCR_DLAB);
	LPC_UART3->IER |= BV(IER_THRE);
	NVIC_EnableIRQ(UART3_IRQn);
}

void uart_puts(char str[])
{
	while(!tx_completed)
		;
	tx_completed=0;
	tx_str = str;	
	tx_index=0;
	LPC_UART3->THR =tx_str[tx_index];
	tx_index++;
}

void UART3_IRQHandler(void)
{
	uint32_t iid ,iir=LPC_UART3->IIR;
	iid = IIR_IID(iir);
	switch(iid)
	{
	case IID_RLS:
		break;
	case IID_RDA:
		break;
	case IID_CTI:
		break;
	case IID_THRE:
		if(tx_str[tx_index] != '\0')
		{
			LPC_UART3->THR =tx_str[tx_index];
			tx_index++;
		}
		else
		{	
			tx_completed=1;
		}
		break;
	}	
}
