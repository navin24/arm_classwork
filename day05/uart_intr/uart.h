#ifndef __UART_H
#define __UART_H

#include "LPC17xx.h"

#define LSR_RDR  0
#define LSR_THRE 5
#define LCR_DL0  0
#define LCR_DL1  1
#define	LCR_STOP 2
#define LCR_PAREN 3  
#define LCR_DLAB 7
#define FIFO_EN 0

#define IER_RBR 0
#define IER_THRE 1
#define IER_RLS 2

#define IIR_PEND 0
#define IIR_IID(iir) ((iir >> 1) & 0x07)
#define IID_RLS 3
#define IID_RDA	2
#define IID_CTI	6
#define IID_THRE 1

void uart_init(uint32_t baud);
void uart_puts(char str[]);
void UART3_IRQHandler(void);




#endif
