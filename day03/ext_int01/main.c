#include "LPC17xx.h"
#include "lcd.h"
#include <stdio.h>

#define EXT2 2

void eint2_init(void);
int eint2_cnt = 0;

int main(void)
{
	lcd_init();
	lcd_puts(LCD_LINE1,"'Interrupt Demo'");
	eint2_init();
	return 0;
}

void eint2_init(void)
{
	LPC_PINCON->PINSEL4 &= ~(BV(25) | BV(24));
	LPC_PINCON->PINSEL4 |= BV(24);

	LPC_SC->EXTMODE |= BV(EXT2);
	LPC_SC->EXTPOLAR |= BV(EXT2);
	LPC_SC->EXTINT |= BV(EXT2);

	NVIC_EnableIRQ(EINT2_IRQn);
}
void EINT2_IRQHandler(void)
{
	char str[20];
	eint2_cnt++;
	sprintf(str,"Int Count:	%2d",eint2_cnt);
	lcd_puts(LCD_LINE2,str);
	LPC_SC->EXTINT |= BV(EXT2);
}
